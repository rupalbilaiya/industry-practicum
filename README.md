# Impact of Learning Interventions on Academic Gains as an Answer to the COVID-19 Pandemic: An Analytics Case Study with the Top School District in Indiana

![image.png](./image.png)

The COVID-19 pandemic disrupted education worldwide. Students missed out on in-person instruction and their grades saw a sharp decline. Our client — a school district in Indiana with more than 15,000 students — looked for creative ways to combat learning loss. In order to increase academic gains in this new environment with pre-existing systemic inequities, our client implemented two sets of treatments: summer school and three concurrent tutoring interventions. 4,500 students attended summer school and 700 students received some kind of tutoring. Tutoring was offered through school teachers (most expensive), contract teachers, and an external vendor (least expensive). Through this study, our client would like to measure the effect of each of these treatments on students’ academic performance.
## Authors

- [@Rupal Bilaiya](rupal.bilaiya@gmail.com)
- [@Michael Jonelis](https://www.linkedin.com/in/michaeljonelis/)
- [@Vivek Rao](https://www.linkedin.com/in/vivek-rao-analytics/)
- [@Alejandro](https://www.linkedin.com/in/alejandrobrillembourg/)
- [@Sriya Musunuru](https://www.linkedin.com/in/sriya-musunuru-271391170/?originalSubdomain=in)


## Tech Stack

**Language:** Machine Learning Algorithms, Python, R

**Tools:** Pycharm, Rstudio, Jupyter Notebook


## Project Description

Paper:
https://app.box.com/s/4e7q5l2xq6p8tkrhugt3cdqbmrnwhgk0

Poster:
https://app.box.com/s/m1s9pu8n5semjpcbnfdjz7w7czef67t2

Video Presentation:
https://youtu.be/kbfUN0mr7ZA
## Documentation

[GITLAB Project](https://gitlab.com/rupalbilaiya/industry-practicum)

